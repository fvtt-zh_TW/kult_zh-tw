#### 基礎設定 ####
# Configure Rolls
rollFormula = "2d10"

# Define roll result ranges.
[rollResults]
[rollResults.success]
range = "15+"
label = "成功!"
[rollResults.partial]
range = "10-14"
label = "有代價的成功"
[rollResults.failure]
range = "9-"
label = "後果"


#### 角色數據 ####

[character.stats]
fortitude = "強韌"
willpower = "意志"
reflexes = "反射"
reason = "理性"
perception = "感知"
intuition = "直覺"
coolness = "冷靜"
charisma = "魅力"
violence = "暴力"
soul = "靈魂"

#### 上方區塊 ####
[character.attributesTop]
[character.attributesTop.armor]
type = "Number"
label = "護甲"

[character.attributesTop.improvement]
  type = "Xp"
  label = "經驗值"
  max = 5
  default = 0

#### 左側區塊 ####
[character.attributesLeft]
[character.attributesLeft.harmConditions]
type = "ListMany"
label = "傷勢"
description = "多個重傷也只承受（持續 -1）；致命傷（持續 -1）"
condition = true
options = [
"重傷",
"重傷",
"重傷",
"重傷",
"致命傷"
]

[character.attributesLeft.conditions]
type = "ListMany"
label = "穩定度"
description = "中等壓力（不安-難以專心）：-1 到劣勢檢定；嚴重壓力（動搖-神經質）：−1 保持沉著、−2 到劣勢檢定；致命壓力（焦慮-精神錯亂）：−2 保持沉著、−3 到劣勢檢定、+1 看穿幻象；崩潰：GM 進行舉措："
condition = true
options = [
"安穩",
"不安",
"難以專心",
"動搖",
"苦惱",
"神經質",
"焦慮",
"不理性",
"精神錯亂",
"崩潰"
]

[character.attributesLeft.look]
type = "LongText"
label = "外觀"

[character.attributesLeft.relation]
type = "LongText"
label = "關係"

[character.attributesLeft.darksecret]
type = "LongText"
label = "黑暗秘密"

[character.attributesLeft.hook]
type = "LongText"
label = "戲劇性的引子"

[character.moveTypes]
playerMoves = "玩家舉措"
advMoves = "優勢舉措"
devMoves = "劣勢舉措"
otherMoves = "其它舉措"

[character.equipmentTypes]
item = "物品"

[npc.attributesTop]
[npc.attributesTop.armor]
type = "Number"
label = "護甲"
[npc.attributesTop.harm]
label = "傷勢"
type = "Clock"
max = 10
default = 0

[npc.attributesLeft.drive]
	type = "LongText"
    label = "數據"

[npc.moveTypes]
hard = "硬舉措"
soft = "軟舉措"